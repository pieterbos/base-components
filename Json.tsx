export const Json = ({ data, title }: { data?: any; title?: string }) => {
  return (
    <div>
      <strong>{title}</strong>
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </div>
  );
};
