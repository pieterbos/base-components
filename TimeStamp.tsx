interface IProps {
  timestamp: number;
}
export const TimeStamp = ({ timestamp }: IProps) => {
  let date: string;

  if (timestamp) {
    console.log(timestamp * 1000);
    date = new Date(timestamp * 1000).toUTCString();
  }
  if (!date) return null;
  return <div>{date}</div>;
};
