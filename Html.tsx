import React from "react";

export const Html = ({ content, className }) => {
  if (!content) {
    return null;
  }
  return (
    <div
      className={className}
      dangerouslySetInnerHTML={{
        __html: content.html,
      }}
    ></div>
  );
};
